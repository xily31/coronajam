﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using TMPro;

public class UIManager : MonoBehaviour
{
    public static UIManager instance = null;        //Declaring and initializing a public static Manager 
    public GameObject _selectedEntity;      //Last entity clicked onto

    public TextMeshProUGUI _selectedEntityText;


    private void Awake()
    {
        //Singleton part
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))    //If left-clic
        {

            RaycastHit2D hit = Raycast2DWithOrder(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);    //Cast a ray on mouse cursor    

            if (hit.collider != null)   //If ray hits something with a collider component
            {
                Debug.Log("Clic detected on: " + hit.collider.gameObject.name);     //Display hit object's name

                switch (hit.collider.tag)
                {
                    case "Robot":
                        GameObject robot = hit.collider.transform.parent.gameObject;
                        _selectedEntity = robot;
                        _selectedEntityText.text = "Selected entity: " + robot.name;
                        break;
                    case "Ground":
                        if (_selectedEntity != null && _selectedEntity.tag == "Robot")
                        {
                            _selectedEntity.GetComponent<RobotController>().SetDestination(hit.point);
                        }
                        break;
                }
            }
        }
    }

    //Returns the element with a collider at the highest sorting layer
    public static RaycastHit2D Raycast2DWithOrder(Vector2 origin, Vector2 direction, float distance = Mathf.Infinity)
    {
        RaycastHit2D result = new RaycastHit2D();

        LayerMask clickableLayer = (1 << LayerMask.NameToLayer("Ingredients"));
        RaycastHit2D[] hits = Physics2D.RaycastAll(origin, direction);

        if (hits.Length > 0) //Only function if we actually hit something
        {
            string detection = "";
            foreach (RaycastHit2D hit in hits) detection += (", " + hit.transform.name);
            Debug.Log("Collision detected with: " + detection);

            int closestItem = 0; //Set our top hit to a default of the first index in our "hits" array, in case there are no others
            int lowestLayerID = int.MaxValue;
            int highestSortingOrder = int.MaxValue;

            for (int i = 1; i < hits.Length; i++) //Loop for every extra item the raycast hit
            {
                SpriteRenderer mySpriteRenderer = hits[i].transform.GetComponent<SpriteRenderer>();
                TilemapRenderer myTilemapRenderer = hits[i].transform.GetComponent<TilemapRenderer>();

                if (mySpriteRenderer == null && myTilemapRenderer == null)
                {
                    Debug.Log("Banana.");
                    break; // if transform has no SpriteRenderer, we leave it out
                }

                int currentLayerID = 0;
                if (mySpriteRenderer)
                {
                    currentLayerID = mySpriteRenderer.sortingLayerID; //Store SortingLayerID of the current item in the array being accessed

                    if (currentLayerID < lowestLayerID) //If the SortingLayerID of the current array item is lower than the previous lowest
                    {
                        lowestLayerID = currentLayerID; //Set the "Previous Value" to the current one since it's lower, as it will become the "Previous Lowest" on the next loop
                        closestItem = i; //Set our topHit with the Array Index value of the current closest Array item, since it currently has the highest/closest SortingLayerID
                        highestSortingOrder = mySpriteRenderer.sortingOrder; //Store SortingOrder value of the current closest object, for comparison next loop if we end up going to the "else if"
                    }
                    else if (currentLayerID == lowestLayerID)
                    {
                        if (mySpriteRenderer.sortingOrder > highestSortingOrder)
                        { //If SortingLayerID are the same, then we need to compare SortingOrder. If the SortingOrder is lower than the one stored in the previous loop, then update values
                            closestItem = i;
                            highestSortingOrder = mySpriteRenderer.sortingOrder;
                        }
                    }
                }
                else if (myTilemapRenderer)
                {
                    currentLayerID = myTilemapRenderer.sortingLayerID; 

                    if (currentLayerID < lowestLayerID) 
                    {
                        lowestLayerID = currentLayerID; 
                        closestItem = i; 
                        highestSortingOrder = myTilemapRenderer.sortingOrder; 
                    }
                    else if (currentLayerID == lowestLayerID)
                    {
                        if (myTilemapRenderer.sortingOrder > highestSortingOrder)
                        { 
                            closestItem = i;
                            highestSortingOrder = myTilemapRenderer.sortingOrder;
                        }
                    }
                }
            }

            result = hits[closestItem];
        }

        return result;

    }
}
