﻿using UnityEngine;
using System.Collections;
using Pathfinding;


public class RobotController : MonoBehaviour
{
    IAstarAI agent;
    AIPath path;

    public bool drawPath = false;
    public Vector3 targetPosition;

    void OnEnable()
    {
        agent = GetComponent<IAstarAI>();
        path = GetComponent<AIPath>();
        if (agent != null) agent.onSearchPath += Update;
    }

    void OnDisable()
    {
        if (agent != null) agent.onSearchPath -= Update;
    }

    void Update()
    {
        path.drawPath = drawPath;
    }

    //Transforms a point in a normalized point on the grid then assigns it as destination for the agent
    public void SetDestination(Vector2 point)
    {
        if (agent != null)
        {
            Vector2 targetPosition = point;

            float gridAlignedX = Mathf.FloorToInt(targetPosition.x) + 0.5f;
            float gridAlignedY = Mathf.FloorToInt(targetPosition.y) + 0.5f;
            targetPosition = new Vector3(gridAlignedX, gridAlignedY);

            agent.destination = targetPosition;
        }
    }
}
